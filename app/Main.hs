import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import System.Random

import Draw
import Consts(screen_x, screen_y, fps)
import Input

main :: IO ()
main = do 
  r <- newStdGen
  play display 
    white
    fps
    (initialState r)
    drawingFunc
    inputHandler
    updateFunc
    where 
      display = InWindow "PixelRush" (round(screen_x), round(screen_y)) (400, 200)
    



