module Collisions (checkCollisions, checkCollision, gameOver) where

import Consts (BNW(..), GObj(..), State(..),screen_h,spike_high,spike_wide)

checkCollisions :: Float -> Float  -> [GObj] -> Bool
checkCollisions playerX playerY obj =  and ( zipWith checkCollision [(playerX ,playerY)] obj )

checkCollision :: (Float,Float) -> GObj  -> Bool
checkCollision (playerX,playerY) obj = if (playerX >= ((xobj obj) - spike_wide))  
                                            && (playerX <= ((xobj obj) + fromIntegral((amount obj)) * spike_wide)) 
                                            && (playerY <= screen_h + spike_high) then True else False

gameOver :: BNW -> Bool
gameOver world = checkCollisions (x (player world)) (y (player world)) (object world)
