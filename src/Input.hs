module Input (inputHandler) where

import System.Exit
import System.Random
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

import Draw (resetGame)
import Consts (State (..),BNW(..),Preference(..), screen_h,object_speed, easy_difficulty_speed, hard_difficulty_speed)

inputHandler :: Event -> BNW -> BNW
inputHandler event world
    | (isGameOver(preference world)) = case event of
    EventKey (SpecialKey KeyEnter) Up _ _ ->  resetGame world
    _ -> world
    | (isMainMenu (preference world)) = case event of
      EventKey (SpecialKey KeyEnter) Up _ _  ->  world{ preference = (preference world){isMainMenu = False}}
      _ -> world
    | (isDifficult (preference world)) = case event of
      EventKey (Char '1') Down _ _  ->  world{ preference = (preference world){isDifficult = False, difficulty = 1}, speed = easy_difficulty_speed}
      EventKey (Char '2') Down _ _  ->  world{ preference = (preference world){isDifficult= False, difficulty = 2},  speed = object_speed}
      EventKey (Char '3') Down _ _  ->  world{ preference = (preference world){isDifficult = False, difficulty = 3}, speed = hard_difficulty_speed}
      _ -> world
    | (isPause (preference world)) = case event of
      EventKey (Char 'p') Down _ _ -> world {preference = (preference world) {isPause = False}}
      _ -> world
    | otherwise = case event of
      (EventKey (SpecialKey KeySpace) Down _ _) -> 
          if (y (player world)) /= screen_h then world 
                                        else  world{ player = (player world) {isPositive = True,y = (y (player world)) + 1}}
      EventKey (Char 'p') Down _ _ -> world {preference = (preference world) {isPause = True}}
      _ -> world



