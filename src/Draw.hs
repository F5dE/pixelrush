module Draw (initialState, drawingFunc, updateFunc, resetGame ) where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import System.Random
import System.Exit

import Collisions (checkCollisions, checkCollision, gameOver)
import Consts ( State(..), BNW(..), GObj(..),Preference(..), jump_speed, gravity, object_speed, screen_h, jump_h, screen_x
              , screen_y, object_initial_position, objects_initial_amount, fps, player_polygon, objects_initial_difference
              , easy_difficulty_before, medium_difficulty_before, hard_difficulty_before, easy_difficulty_after
              , medium_difficulty_after, hard_difficulty_after, error_handler, spike_high, spike_wide, spike_polygon)
import Gen (spikes, moveObjects, initGameObjects, initObject)

drawingFunc :: BNW -> Picture
drawingFunc world
  | (isMainMenu ( preference world)) = pictures [
     Translate (-135)   (0)   . Scale 0.4 0.4 . Text $ "PixelRush"
    , Translate (-80) (-25) . Scale 0.1 0.1 . Text $ "Press 'Enter' to countinue..."
    ]
  | (isDifficult(preference world)) = pictures 
    [ Translate (-100)   20   . Scale 0.2 0.2 . Text $ "Choose difficulty "
    , Translate (-50) (-20)  . Scale 0.15 0.15 . Text $ "1.  Easy walking"
    , Translate (-50) (-42) . Scale 0.15 0.15 . Text $ "2.  Normal run"
    , Translate (-50) (-66) . Scale 0.15 0.15 . Text $ "3.  UBER"
    , Translate (2) (-48) $ Scale 0.15 0.15 $ circle 8
    , Translate (6) (-48) $ Scale 0.15 0.15 $ circle 8
    ]
  | (isGameOver(preference world)) = pictures
      [ Translate (-100)   (0)  . Color red .Scale 0.2 0.2 . Text $ "Game over "
        ,Translate (-100) (-25)  . Scale 0.2 0.2 . Text $ "Your score: " ++ scoreWorld
        ,Translate (-100) (-50) . Scale 0.1 0.1 . Text $ "Press 'Enter' to restart..."
      ]
  |otherwise = pictures [
    drawObjects (object world),
    drawScore (score world),
    drawPlayer (player world),
    drawPause(isPause(preference world))] 
  where scoreWorld = show . round $ score world

drawPause :: Bool -> Picture
drawPause pause = if (pause == False) then pictures [translate (-220) (110) (Color black $ Scale 120 120 $ Polygon [ (0.15,0.0),(0, 0.1),(0,-0.1)])
                                              , translate (-225) (116) (Color black $ Scale 120 120 $ Polygon [ ( 0.025, 0.05),(-0.025, 0.05),(-0.025, -0.15),( 0.025, -0.15)])
                                              ,translate (130) (105) . Scale 0.1 0.1 . Text $ "pause, press P"] -- draw play symbol
                                     else pictures [translate (-213) (116) (Color black $ Scale 120 120 $ Polygon [ ( 0.025, 0.05),(-0.025, 0.05),(-0.025, -0.15),( 0.025, -0.15)])
                                              , translate (-225) (116) (Color black $ Scale 120 120 $ Polygon [ ( 0.025, 0.05),(-0.025, 0.05),(-0.025, -0.15),( 0.025, -0.15)])
                                              ,translate (130) (105) . Scale 0.1 0.1 . Text $ "resume, press P"]  -- draw pause symbol 

drawPlayer :: State -> Picture 
drawPlayer state = (translate (x state) (y state) (Color black
              $ Scale 120 120
              $ Polygon player_polygon))

drawObject :: GObj -> Float -> Picture  
drawObject obj r = translate x y (Color black
                              $ Scale 120 120
                              $ Polygon spike_polygon)          
                  where 
                    x = (xobj obj) + r
                    y = (yobj obj)      

drawObjectAmount :: GObj -> Picture 
drawObjectAmount obj = if (amount obj) == 1
                        then drawObject obj 0
                  else if (amount obj) == 3
                   then pictures
                    [drawObject obj 0,
                    drawObject obj spike_wide,
                    drawObject obj (spike_wide*2)]    
                  else pictures
                    [drawObject obj 0,
                    drawObject obj spike_wide]  

drawObjects :: [GObj] -> Picture     
drawObjects = pictures . map drawObjectAmount . takeWhile onScreen . spikes
  where
    onScreen obj = (xobj obj)  < screen_x

drawScore :: Float -> Picture 
drawScore score = translate x y (scale 20 20 (pictures
  [translate 2 (-1.5) (scale 0.01 0.01 (color black (text (show (round (score))))))]))
  where
    x = -(screen_x/2) + 20
    y = (screen_y/2) - 25

updateFunc :: Float -> BNW ->  BNW
updateFunc dt world  
                    | (isDifficult (preference world)) =  world
                    | (isMainMenu (preference world)) =  world
                    | (isPause (preference world)) =  world
                    | gameOver world = world{ preference = (preference world){isGameOver = True}}
                    | otherwise = world {
                      player = (player world){ isPositive = checkPositive (y (player world)) (isPositive (player world)),
                      y = (towardInitialState  dt (isPositive (player world)) ( y (player world)))}, 
                      speed = (speed world) + (score world)/
                      (if (score world) < 20 
                        then if (difficulty (preference world)) == 3 then hard_difficulty_before else if (difficulty (preference world)) == 2 then medium_difficulty_before else easy_difficulty_before
                        else if (difficulty (preference world)) == 3 then hard_difficulty_after else if (difficulty (preference world)) == 2 then medium_difficulty_after else easy_difficulty_after
                      ),
                      object = moveObjects (speed world) dt (object world), score = (score world) + dt}
  where
    towardInitialState :: Float -> Bool -> Float -> Float
    towardInitialState dt isPosiv c = if c <= screen_h 
      then screen_h
      else if c >= screen_h + jump_h + error_handler 
        then screen_h + jump_h + error_handler 
      else if (isPosiv == True)
        then c + jump_speed*dt
      else  c - gravity*dt
    checkPositive :: Float -> Bool -> Bool
    checkPositive c b = if (c > screen_h + jump_h) then  False
                      else b

initialState :: StdGen -> BNW
initialState r = BNW{ speed = object_speed
                    , object = initGameObjects r
                    , score = 0
                    , player = State { angle = 0
                                    , isPositive = False
                                    , x = 0
                                    , y = screen_h
                                    , picture
                                        = Color black
                                        $ Scale 120 120
                                        $ Polygon player_polygon
                                    }
                    , preference = Preference { isMainMenu = True
                                              , isGameOver = False
                                              , isDifficult = True
                                              , isPause = False
                                              , difficulty = 1
                                              }             
                    }

resetGame :: BNW -> BNW
resetGame world =  world{ speed = object_speed
          , object = (tail(object world))
          , score = 0
          , player = State { angle = 0
                          , isPositive = False
                          , x = 0 
                          , y = screen_h
                          , picture
                              = Color black
                              $ Scale 120 120
                              $ Polygon player_polygon
                          }
          , preference = (preference world){ isMainMenu = False
                                            , isGameOver = False
                                            , isDifficult = True
                                            , isPause = False
                                            , difficulty = 1
                                            }  
                          }
