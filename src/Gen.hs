module Gen (spikes, moveObjects, initGameObjects, initObject) where 
import Consts ( State(..),BNW(..), jump_speed, gravity, GObj(..), object_speed,  screen_h, jump_h, screen_x
              , screen_y, objects_initial_amount, object_initial_position, objects_initial_difference)
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import System.Random

spikes :: [GObj] -> [GObj]
spikes = go 0
  where
    go _ []            = []
    go s (obj : objs) = obj{xobj = (xobj obj) + s} : go (if s < 2000 then (s + objects_initial_difference + (xobj obj))
                                                          else if s < 10000 then (s + objects_initial_difference*3 + (xobj obj))
                                                          else (s + objects_initial_difference*30 + (xobj obj))) objs

moveObjects :: Float -> Float -> [GObj] -> [GObj]
moveObjects _ _ [] = []
moveObjects sp dt (obj : objs)
    | dx > pos  = objs
    | otherwise = obj{xobj = (xobj obj) - dx} : objs
    where
      pos = (xobj obj) + objects_initial_difference
      dx  = dt * sp 

initObject :: Float -> Int -> GObj
initObject rx ra = GObj{ xobj = rx
                       , yobj = screen_h
                       , amount = ra
                      }

initGameObjects :: StdGen -> [GObj]
initGameObjects r =  zipWith initObject (randomRs object_initial_position r) (randomRs objects_initial_amount r)

