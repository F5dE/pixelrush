module Consts ( State(..), BNW(..), GObj(..),Preference(..), jump_speed, gravity, object_speed, screen_h, jump_h, screen_x
              , screen_y, object_initial_position, objects_initial_amount, fps, player_polygon, objects_initial_difference
              , easy_difficulty_before, medium_difficulty_before, hard_difficulty_before, easy_difficulty_after
              , medium_difficulty_after, hard_difficulty_after, error_handler, spike_high, spike_wide
              , spike_polygon, easy_difficulty_speed, hard_difficulty_speed) where
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

--Spikes
data GObj = GObj { xobj :: Float
                 , yobj :: Float
                 , amount :: Int}

-- Game settings
data Preference = Preference{ isMainMenu :: Bool
                            , isGameOver :: Bool
                            , isDifficult :: Bool
                            , isPause :: Bool
                            , difficulty :: Int}      

--BraveNewWorld 
data BNW = BNW { speed :: Float
               , object :: [GObj] 
               , score :: Float
               , player :: State
               , preference :: Preference}

-- Player
data State = State { angle :: Float
                   , isPositive :: Bool
                   , x :: Float
                   , y :: Float
                   , picture :: Picture }

-- Free fall
gravity :: Float
gravity = 200

-- After jump player speed
jump_speed :: Float
jump_speed = 200

-- Initial spikes speed
object_speed :: Float 
object_speed = 150

-- Player initial position
screen_h :: Float
screen_h = -100

-- Jump Height
jump_h :: Float
jump_h = 120

-- Error handler
error_handler :: Float
error_handler = 50

-- Screen dimension x
screen_x :: Float
screen_x = 500

-- y
screen_y :: Float
screen_y = 250

-- Random interval for spikes
object_initial_position :: (Float,Float)
object_initial_position = (350, 850)

-- Random amount of spikes in a row
objects_initial_amount :: (Int, Int)
objects_initial_amount = (1,3)

fps :: Int
fps = 120

-- Polygon of Box
player_polygon :: [(Float, Float)]
player_polygon = [ ( 0.10, 0.10)
          , (-0.10, 0.10)
          , (-0.10, -0.10)
          , ( 0.10, -0.10)]

spike_polygon :: [(Float,Float)]
spike_polygon = [ (0.0,0.1)
                , (-0.10, -0.10)
                , (0.10,-0.10)]

-- Spikes x difference in the world
objects_initial_difference :: Float
objects_initial_difference = 300

-- Difficulties
easy_difficulty_before :: Float
easy_difficulty_before = 300

medium_difficulty_before :: Float
medium_difficulty_before = 200

hard_difficulty_before :: Float
hard_difficulty_before = 150

easy_difficulty_after :: Float
easy_difficulty_after = 1000

medium_difficulty_after :: Float
medium_difficulty_after = 650

hard_difficulty_after :: Float
hard_difficulty_after = 450

easy_difficulty_speed :: Float
easy_difficulty_speed = 100

hard_difficulty_speed :: Float
hard_difficulty_speed = 350

-- Spikes generator
spike_high :: Float
spike_high = 10

spike_wide :: Float
spike_wide = 25


